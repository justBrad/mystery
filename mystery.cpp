#include <iostream>
#include <vector>
using namespace std;

void print(auto A)
{
   for (auto a : A) 
        cout <<a<<" ";

   cout<<endl;
}

void mystery1(auto& Data)
{
  cout<<endl<<"Mystery 1"<<endl<<"---------------------"<<endl;

  for ( int i = 0 ; i < (int)Data.size( ) ; i++)
  {
    for ( int j = 0 ; j < i ; j++)
	if ( Data[ i ] < Data[ j ] )
	    swap( Data[ i ] , Data[ j ] );

    print(Data);
  }//end outer for (this brace is needed to include the print statement)

}

//... Other mysteries...
void mystery2(auto& Data){
  cout<<endl<<"Mystery 2"<<endl<<"---------------------"<<endl;
  int sortIndex = 0, unsortedIndex = 0, minIndex = 0;
  for (sortIndex = 0; sortIndex < (int)Data.size() - 1; sortIndex++){
    minIndex = sortIndex;
    for (unsortedIndex = sortIndex + 1; unsortedIndex < (int)Data.size(); unsortedIndex++){
      if (Data[unsortedIndex] < Data[minIndex])
        minIndex = unsortedIndex;
    }
    if (minIndex != sortIndex){
      swap(Data[sortIndex], Data[minIndex]);
    }
    print(Data);
  }  
}

void mystery3(auto& Data){
  cout<<endl<<"Mystery 3"<<endl<<"---------------------"<<endl;
  int nextIndex = 0, moveItem = 0, insertValue = 0;
  for (nextIndex = 1; nextIndex < (int)Data.size(); nextIndex++){
    insertValue = Data[nextIndex];
    moveItem = nextIndex;
    while (moveItem > 0 && Data[moveItem - 1] > insertValue){
      Data[moveItem] = Data[moveItem - 1];
      moveItem--;
    }
    Data[moveItem] = insertValue;
    print(Data);
  }
}

int main()
{
    
  vector<int> Data = {36, 18, 22, 30, 29, 25, 12};

  vector<int> D1 = Data;
  vector<int> D2 = Data;
  vector<int> D3 = Data;

  mystery1(D1);
  mystery2(D2);
  mystery3(D3);

}